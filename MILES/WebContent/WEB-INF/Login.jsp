<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<h2>Struts 2 - Login Application</h2>
<s:actionerror />
<s:form action="login">
	<s:textfield name="username" label="Name"></s:textfield>  
	<s:password name="userpass" label="Password"></s:password>  
	<s:submit value="login"></s:submit>  
</s:form>
