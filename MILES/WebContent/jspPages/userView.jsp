<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
String _startPoint=request.getAttribute("startPt").toString();
String _endPoint=request.getAttribute("_endPt").toString();
String _device_id=session.getAttribute("device_id").toString();
String _bw[]=(String[])request.getAttribute("_betweenPT");
request.setAttribute("s",_startPoint);
request.setAttribute("e",_endPoint);
request.setAttribute("b",_bw);
String start=session.getAttribute("start").toString();
out.print(start);
%>
<table border="1">
	<tr>
		<td>ID</td>
		<td>START</td>
		<td>END</td>
		<td>CALCULATE</td>		
	</tr>
	<tr>
		<td><%=_device_id%></td>
		<td><%=_startPoint %></td>
		<td><%=_endPoint %></td>
		<td><a href="Direction?st=<%=_endPoint %>&ed=<%=_endPoint %>&bw=<%=_bw%>">Calculate</a></td>
	</tr>
</table>
</body>
</html>