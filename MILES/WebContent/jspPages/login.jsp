<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- Using Struts2 Tags in JSP --%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<h3>Welcome User, please login below</h3>
<s:if test="hasActionMessages()">
	<s:actionmessage />
</s:if>

<%
String msg = (String)request.getAttribute("logoutMsg");
 if(msg !=null){
	out.print(msg+"<br/><br/>");
} 
%>
<s:form action="loginAuth">
	<s:textfield name="employeesBean.EmpUserId" label="User Name"></s:textfield>
	<s:password name="employeesBean.FirstName" label="Password"
		type="password"></s:password>
	<s:submit value="Login"></s:submit>
</s:form>