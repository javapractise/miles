<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in directions</title>
    <style>
      #right-panel {
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }

      #right-panel select, #right-panel input {
        font-size: 15px;
      }

      #right-panel select {
        width: 100%;
      }

      #right-panel i {
        font-size: 12px;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
        float: left;
        width: 70%;
        height: 100%;
      }
      #right-panel {
        margin: 20px;
        border-width: 2px;
        width: 20%;
        float: left;
        text-align: left;
        padding-top: 20px;
      }
      #directions-panel {
        margin-top: 20px;
        background-color: #FFEE77;
        padding: 10px;
      }
    </style>
  </head>
  <%
  String start=session.getAttribute("start").toString();
  String end=session.getAttribute("end").toString();
  String bw[]=(String[])session.getAttribute("between");
  
  %>
  <body>
    <div id="map"></div>
    <div id="right-panel">
    <div>
    <b>Start:</b>
    <select id="start">
      <option value="<%=start%>">Velocis</option>
      
    </select>
    <br>
    <b>Waypoints:</b> <br>
    <i>(Ctrl-Click for multiple selection)</i> <br>
    <select multiple id="waypoints" style="">
     <option value="28.603971, 77.368513" selected="selected">28.603971, 77.368513</option>
      <option value="28.520496, 77.388754" selected="selected">Sector 93</option>
      
    </select>
    <select multiple id="waypoint" style="display: none;">
    
    <%
	
   
		for(String prcss:bw)
			{
	%>
			<option value="<%=prcss%>" selected="selected"><%=prcss %></option>
		<%
			} 
		%>
	</select>
    <br>
    <b>End:</b>
    <select id="end">
      <option value="<%=end%>">sector 59</option>
    </select>
    <br>
      <input type="submit" id="submit" onclick="initMap()">
    </div>
    <div id="directions-panel"></div>
    </div>
    <script>
    var totalKm=0;
      function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 6,
          center: {lat: 41.85, lng: -87.65}
        });
        directionsDisplay.setMap(map);
        calculateAndDisplayRoute(directionsService, directionsDisplay);
        
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    	
        var waypts = [];
        var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
            waypts.push({
              location: checkboxArray[i].value,
              stopover: true
            });
            
          }
        }

        directionsService.route({
          origin: document.getElementById('start').value,
          destination: document.getElementById('end').value,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
			 var ii = 0;
            for (var i = 0; i < route.legs.length; i++) {
              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                  '</b><br>';
              summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
              summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';

             
			  calculateKM(route.legs[i].distance.text);
			  //alert("goes "+ii); 
			  
            }
			//alert(ii);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
      function calculateKM(ii)
      {
    	var data=ii.trim();
    	var len=data.length;
    	var indexOfKm=data.indexOf("km",0);
    	
    	
    	if(indexOfKm!=-1)
    	{
    		var removeKm=data.substring(0,len-2);
    		totalKm=parseFloat(totalKm)+parseFloat(removeKm);
    	}
    	else
    	{
    		var removeM=data.substring(0,len-1);
    		totalKm=parseFloat(totalKm)+parseFloat(removeM)/1000;	
    	}
    	
    	alert("result "+totalKm);
    	
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoA_NnxV2b5LeocC6GAVv1uUutBguDsHM&callback=initMap">
    </script>
  </body>
</html>