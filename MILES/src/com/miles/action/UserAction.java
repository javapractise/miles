package com.miles.action;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.miles.bo.UserBO;
import com.miles.dao.VException;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport implements SessionAware, ServletRequestAware {
	
  @SuppressWarnings("rawtypes")
  HashMap szHmap=null;
  @SuppressWarnings("rawtypes")
  Vector szVec=null;
  UserBO szUserBO=null;
  private SessionMap<String,Object> sessionMap;  
  HttpServletRequest szRequest = ServletActionContext.getRequest();
  
  
@SuppressWarnings({ "rawtypes", "unchecked" })
public String userDetailsView() throws VException ,ParseException{
		szVec = new Vector();
		szHmap = new HashMap();
		HttpServletRequest _request = ServletActionContext.getRequest();
		HttpSession szSession = ServletActionContext.getRequest().getSession();
		szUserBO = new UserBO(szRequest);
		HashMap _hmap=new HashMap();
		try{
			szHmap = new HashMap();
			String _device_id=szSession.getAttribute("device_id").toString();
			System.err.println("_device_id_device_id_device_id_device_id"+_device_id);
			szHmap.put("USER_ID", _device_id); 
			szVec=szUserBO.getDistanceDetails(szHmap);
			int freqOf8=(szVec.size()-2)/7;
			System.err.println("frequenct"+freqOf8);
			int valueFromIndex=freqOf8;
			System.err.println("szVecszVecszVecszVec"+szVec);
			_hmap=(HashMap)szVec.get(0);
			String _latitude=_hmap.get("LATITUDE").toString();
			String _longitude=_hmap.get("LONGITUDE").toString();
			String _startPt=_latitude+","+_longitude;
			_hmap.clear();
			_hmap=(HashMap)szVec.get(szVec.size()-1);
			String _elatitude=_hmap.get("LATITUDE").toString();
			String _elongitude=_hmap.get("LONGITUDE").toString();
			String _endPt=_elatitude+","+_elongitude;
			_request.setAttribute("startPt", _startPt);
			_request.setAttribute("_endPt", _endPt);
			System.out.println("szVec user action"+szVec.size());
			ArrayList _betweenPT=new ArrayList();
			for(int i=1;i<=szVec.size()-2;i++)
			{
				if(valueFromIndex<=(szVec.size()-2))
				{
					HashMap _map=(HashMap) szVec.get(valueFromIndex);
					valueFromIndex=valueFromIndex+freqOf8;
					String la=_map.get("LATITUDE").toString();
					String lo=_map.get("LONGITUDE").toString();
					String _finalPoint=la+","+lo;
					_betweenPT.add(_finalPoint);
				}
				
			}
			Object[] objectArray=_betweenPT.toArray();
			String[] _myArray = Arrays.copyOf(objectArray, objectArray.length, String[].class);
			_request.setAttribute("_betweenPT", _myArray);
			szSession.setAttribute("start",_startPt);
			szSession.setAttribute("end",_endPt);
			szSession.setAttribute("between",_myArray);
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		return "success";	
	
}
public String direction()
{
	return "success";
}

@Override
public void setSession(Map<String, Object> arg0) {
	this.sessionMap=(SessionMap<String, Object>) arg0;
	
}


@Override
public void setServletRequest(HttpServletRequest request) {
	this.szRequest= request;
	
}


public SessionMap<String, Object> getSessionMap() {
	return sessionMap;
}



}
