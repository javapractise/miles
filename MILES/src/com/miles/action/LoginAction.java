package com.miles.action;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.miles.bo.LoginActionBO;
import com.miles.dao.VException;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class LoginAction extends ActionSupport{
	HttpServletRequest szRequest = ServletActionContext.getRequest();
	LoginActionBO szLoginBO=null;
	private String szUsername;
	private String szUserpass;
    HashMap szHMap;
   	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String authenticateUser() {
		 HttpSession _session = ServletActionContext.getRequest().getSession();
    boolean _flag=false;
		String _userName=this.getSzUsername();
		szLoginBO=new LoginActionBO(szRequest);
		try {
			szHMap=new HashMap();
			szHMap.put("device_id", _userName);
			System.out.println("_userName "+_userName);
			_session.setAttribute("device_id", _userName);
			_flag=szLoginBO.authenticate(szHMap);
			
		} catch (VException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("_userName"+_userName+"_flag "+_flag);
		if(_flag){
			
		   return "success";
		}
		else
			return "login";
	}

	public String getSzUsername() {
		return szUsername;
	}

	public void setSzUsername(String szUsername) {
		this.szUsername = szUsername;
	}

	public String getSzUserpass() {
		return szUserpass;
	}

	public void setSzUserpass(String szUserpass) {
		this.szUserpass = szUserpass;
	}
 

}