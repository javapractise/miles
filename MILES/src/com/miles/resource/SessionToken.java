package com.miles.resource;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author vivekg
 */
public class SessionToken {
    
    /**
     * Creates a new instance of SessionToken
     */
    public SessionToken() {
    }
    
    /*
     * Within this class all setter & getters methods will be provided
     * We will set the attributes in Session from its getters and setters methods
     */
    private String szUsername;
	private String szUserpass;
	 private String szRole;
	public String getSzRole() {
		return szRole;
	}
	public void setSzRole(String szRole) {
		this.szRole = szRole;
	}
	public String getSzUsername() {
		return szUsername;
	}
	public void setSzUsername(String szUsername) {
		this.szUsername = szUsername;
	}
	public String getSzUserpass() {
		return szUserpass;
	}
	public void setSzUserpass(String szUserpass) {
		this.szUserpass = szUserpass;
	}
   
   

    }
