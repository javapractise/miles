package com.miles.bo;

import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import com.miles.dao.VException;

public class UserBO extends BusinessObject{
	private final Logger szLog = Logger.getLogger(UserBO.class);
	HttpServletRequest szRequest;
	@SuppressWarnings("rawtypes")
	HashMap szHMap;
	
	public UserBO(HttpServletRequest l_request)
	{
		this.szRequest = null;
		szHMap = null;
		this.szRequest = l_request;
	}

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public Vector getDistanceDetails(HashMap l_hMap) throws VException
	{
		Vector _vecData=null; 
	  try
		{
			_vecData=new Vector();
			_vecData = dao.retrieve("userDisCal", l_hMap);
			System.out.println((new StringBuilder()).append("userDisCal").append(_vecData).toString());
			System.out.println("_vecData.size() "+_vecData.size());
		
			
		}
		catch(VException ve)
		{
			szLog.error((new StringBuilder()).append("error in retrieveing data").append(ve).toString());
		}
		catch(Exception ex)
		{
			szLog.error((new StringBuilder()).append("error in getDistanceDetails").append(ex).toString());
		}
		szLog.info("exiting getDistanceDetails");
		return _vecData;
	}

}
