package com.miles.bo;


import org.apache.log4j.Logger;
import com.miles.dao.DAO;
import com.miles.dao.VException;

/**
 *
 * @author vivekg
 */
public abstract class BusinessObject {
    private final Logger _log = Logger.getLogger(AccessCheck.class);
   
    public DAO dao = null;
    public boolean isValidConnection=false;
        /*
         * Creates a new instance of BusinessObject
         * In this constructor we are creating a new instance of DAO_2
         * we cre opening the connection of DAO_2
         */
    public BusinessObject() {
        _log.info("Entering constructor of BusinessObject");
        try {
            dao = new DAO();
            isValidConnection = dao.openConnection();
            _log.info("Connection open in Business Object");
        } catch (VException ex) {
            _log.error("VException in open connection: "+ex.getMessage());
        } catch (Exception ex) {
            _log.error("Exception in open connection: "+ex.getMessage());
        }
        _log.info("Exiting constructor of BusinessObject");
    }
       /*
        * to close the connection used in query execution &
        * to nullify the objects used in BO
        */
    public void close(){
        _log.info("Entering close() method of BusinessObject");
        try {
            dao.closeConnection();
            _log.info("Connection closed");
        } catch (VException ex) {
            _log.error("VException in close connection: "+ex.getMessage());
        } catch (Exception ex) {
            _log.error("Exception in close connection: "+ex.getMessage());
        }
        _log.info("Exiting close() method of BusinessObject");
    }
}
