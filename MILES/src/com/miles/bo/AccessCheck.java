package com.miles.bo;


import com.miles.dao.VException;
import com.miles.resource.SessionToken;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author vivekg
 */
public class AccessCheck{
    private final Logger _log = Logger.getLogger(AccessCheck.class);
    private HttpServletRequest _request = null;
        /*
         * this method will check the authorization
         */
    public boolean isAccessAllowed(HttpServletRequest _request, String _activity) throws VException{
        _log.info("inside isAccessAllowed in AccessCheck");
        this._request = _request;
        boolean _flag = false;
        try{
             /*
              * getting activity name from request to which authorization will be checked
              * getting the roles from the specified Resource Bundle
              */
            HttpSession _session = _request.getSession();
            if(_activity == null || _activity.trim().equals("")){
                _log.error("Please set activity in Request object");
                throw new VException("Please set activity in Request object");
            }else{
                _log.debug("activity is "+_activity);
                String str_role = ResourceBundle.getBundle("com/resource/SecurityCheck").getString(_activity);
                _log.debug("Related roles are "+str_role);
              /*
               * sessionToken is the bean that is added in session
               * getting the roles from that session
               * a user can contain more than one role seperated by '~'
               * first tokenize it and than match it with the defined authorization for that page
               */
                SessionToken sessionToken = (SessionToken) _session.getAttribute("SessionToken");
                String userRole = sessionToken.getSzRole();
                if(str_role.endsWith("~")){
                    if(userRole != null){
                        StringTokenizer st_user = new StringTokenizer(userRole,"~");
                        String[] user_role = new String[st_user.countTokens()];
                        int _next=0;
                        while (st_user.hasMoreTokens()) {
                            user_role[_next++]=st_user.nextToken();
                        }
                        for (int outer = 0; outer < user_role.length; outer++) {
                            if(str_role.contains(user_role[outer]+"~")){
                                _flag = true;
                                _log.debug("role matches");
                                return _flag;
                            }
                        }
                    }else{
                        _log.error("There is no role defined in session");
                        throw new VException("There is no role defined in session");
                    }
                }else{
                    _log.error("Your roles in property file should end with a '~'");
                    throw new VException("Your roles in property file should end with a '~'");
                }
            }
        }catch(Exception ie){
            _log.error("Exception in accessing roles: "+ie.getMessage());
            throw new VException("Exception in accessing roles: "+ie.getMessage());
        }
        _log.info("exiting isAccessAllowed in AccessCheck");
        return _flag;
    }
}
