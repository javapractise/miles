package com.miles.bo;

import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import com.miles.dao.VException;

public class LoginActionBO extends BusinessObject{
	private final Logger szLog = Logger.getLogger(LoginActionBO.class);
	HttpServletRequest szRequest;
	@SuppressWarnings("rawtypes")
	HashMap szHMap;
	
	public LoginActionBO(HttpServletRequest l_request)
	{
		this.szRequest = null;
		szHMap = null;
		this.szRequest = l_request;
	}

	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public boolean authenticate(HashMap l_hMap) throws VException
	{
		Vector _vecData=null;
		boolean _flag = false;
		szLog.info("Entering authenticate()");
		try
		{
			_vecData=new Vector();
			_vecData = dao.retrieve("userAuthQuery", l_hMap);
			System.out.println((new StringBuilder()).append("userAuthQuery").append(_vecData).toString());
			System.out.println("_vecData.size() "+_vecData.size());
			if(_vecData.size()>0){
				_flag=true;
			}
		}
		catch(VException ve)
		{
			szLog.error((new StringBuilder()).append("error in retrieveing data").append(ve).toString());
		}
		catch(Exception ex)
		{
			szLog.error((new StringBuilder()).append("error in authenticate").append(ex).toString());
		}
		szLog.info("exiting authenticate");
		return _flag;
	}

}
