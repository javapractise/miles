/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miles.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.naming.Context;
import javax.sql.DataSource;

import org.apache.log4j.Logger;


public class DAO implements DAOInterface {

	private static final Logger _log = Logger.getLogger(DAO.class);
	//PropertyConfigurator.configure("Dao_log4j.properties");   
	private Connection _con = null;
	/*
	 * Method for Creating a Connection
	 */

	@Override
	public boolean openConnection() throws VException {
		_log.info("inside openConnection method");
		boolean _status = false;
		try {
			//Class.forName("com.mysql.jdbc.Driver").newInstance(); 
			System.out.println("Driver Loaded.");

			//_con=DriverManager.getConnection("jdbc:sqlserver://192.168.50.5:1433;databaseName=ITMSDB","sa","password@123");//for velocis
			//_con=DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=ITMSDB","sa","pass@123");//for UAT
			//_con=DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=ITMSDB","sa","lokesh2406");//for mylocal
			//_con=DriverManager.getConnection("jdbc:sqlserver://10.1.5.59:1433;databaseName=VEDL-ODAF","LMS","Password@678");//for production server
			//_con=DriverManager.getConnection("jdbc:mysql://192.168.50.57:3306/miles","root","root");  
			
			DataSource source = null;
            javax.naming.InitialContext ctx = new javax.naming.InitialContext();
            Context envContext = (Context) ctx.lookup("java:comp/env");
            source = (javax.sql.DataSource) envContext.lookup("jdbc/miles");
            _con = source.getConnection();
        
			System.out.println("Connection Created.\n");
			_log.debug("connection created is:" + _con);
			_status = true;

		}  catch (SQLException sqle) {
			System.out.println("Exception" + sqle);
			_log.error("SQLException:" + sqle.getMessage());
			_status = false;
			throw new VException("openConnection() Error: " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			System.out.println("Exception" + e);
			_log.error("Exception:" + e.getMessage());
			_status = false;
			throw new VException("openConnection() Error: " + e.getMessage() + " Please contact administrator");
		}
		_log.info("exiting openConnection method");
		return _status;
	}
	/*
	 * Method for closing connection
	 */

	@Override
	public void closeConnection() throws VException {
		_log.info("inside closeConnection method: ");
		if (_con != null) {
			try {
				_con.close();
			} catch (SQLException sqle) {
				_log.error("SQLException:" + sqle.getMessage());
				throw new VException("Connection close Error: " + sqle.getMessage() + " Please contact administrator");
			} catch (Exception ex) {
				_log.error("Exception:" + ex.getMessage());
				throw new VException("Connection close Error: " + ex.getMessage() + " Please contact administrator");
			}
			_con = null;
		}
		_log.info("exiting closeConnection method");
		System.out.println("\nConnection Closed.");
	}
	/*
	 * Method for fetching the query & looks for the query string
	 * _queryID : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	public String fetchQuery(String _queryID, HashMap _params) throws VException {
		_log.info("inside fetchQuery method: ");
		String _query = null;
		try {
			_query = ResourceBundle.getBundle("com/miles/dao/Query").getString(_queryID);
			_log.debug("query is:" + _query);
			int _countQueryScriptlet = (_query.split("<%~").length) - 1;
			_log.debug("count of query scriptlet" + _countQueryScriptlet);
			for (int _incr = 0; _incr < _countQueryScriptlet; _incr++) {
				if (_query.contains("<%~")) {
					String _key = _query.substring(_query.indexOf("<%~") + 3, _query.indexOf("~%>"));
					_log.debug("key for query string is" + _key);
					String _value = null;
					if (_params.containsKey(_key)) {
						_value = (String) _params.get(_key);
						_query = _query.replace("<%~" + _key + "~%>", _value);
						_log.debug("Query after replacing query scriptlet is: " + _query);
					} else {
						_log.error("There is no value in hashmap for the key " + _key);
						throw new VException("There is no value in hashmap for the key " + _key);
					}
					_key = _value = null;
				}
			}
			_queryID = null;
		} catch (Exception ex) {
			_query = null;
			_log.error("Error in fetchQuery(): " + ex.getMessage());
			throw new VException("Error in fetchQuery(): " + ex.getMessage());
		}
		_log.info("exiting fetchQuery method");
		return _query;
	}
	/*
	 * This will parse the date or timestamp into a defined format
	 * format will be defined in Query.properties
	 * date_time : Date or Time to be formatted
	 * _type : tell us if it is a Date or TimeStamp
	 */

	public boolean setParams(String _queryID, PreparedStatement pstmt, HashMap _params) throws VException {
		_log.info("inside setParams method: ");
		boolean _ifParamSet = false;
		StringTokenizer _st = null;

		// Fetching params from query.properties
		String query_params = ResourceBundle.getBundle("com/miles/dao/Query").getString(_queryID + "-params");
		_log.debug("params from property file are:" + query_params);

		if (!query_params.equals("") && query_params != null) {
			_st = new StringTokenizer(query_params, "#");
			_log.debug("Total params set in property file for this query is:" + _st.countTokens());
			_log.debug("Total params set in provided hash map for this query is:" + _params.size());
			int param_position = 0;
			while (_st.hasMoreTokens()) {
				param_position++;
				String _token = _st.nextToken();
				String _key = _token.substring(0, _token.indexOf("~"));
				String data_type = _token.substring(_token.indexOf("~") + 1, _token.length());
				_log.debug("key is:" + _key + " & data_type is:" + data_type);
				boolean _keyExist = _params.containsKey(_key);
				if (_keyExist) {
					if (!data_type.equals("") && data_type != null) {
						try {
							if (data_type.equals("S")) {
								pstmt.setString(param_position, (String) _params.get(_key));
							} else if (data_type.equals("I")) {
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setInt(param_position, Integer.valueOf(0));
								} else {
									pstmt.setInt(param_position, Integer.valueOf(_params.get(_key).toString()));
								}
							} else if (data_type.equals("D")) {
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setDate(param_position, null);
								} else {
									Date util_date = parseIntoDate(_params.get(_key).toString(), "D");
									java.sql.Date _date = new java.sql.Date(util_date.getTime());
									pstmt.setDate(param_position, _date);
								}
							} else if (data_type.equals("T")) {
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setTimestamp(param_position, null);
								} else {
									Date util_date = parseIntoDate(_params.get(_key).toString(), "T");
									java.sql.Timestamp _tstamp = new java.sql.Timestamp(util_date.getTime());
									pstmt.setTimestamp(param_position, _tstamp);
								}
							} else if (data_type.equals("F")) {
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setFloat(param_position, Float.valueOf(0));
								} else {
									pstmt.setFloat(param_position, Float.valueOf(_params.get(_key).toString()));
								}
							} else if (data_type.equals("Db")) {
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setDouble(param_position, Double.valueOf(0));
								} else {
									pstmt.setDouble(param_position, Double.valueOf(_params.get(_key).toString()));
								}
							}else if (data_type.equals("B")) {
								InputStream is = null;
								if (_params.get(_key).equals("") || _params.get(_key) == null) {
									pstmt.setBlob(param_position, is);
								} else {
									is = (InputStream) _params.get(_key);
									pstmt.setBlob(param_position, is);
								}
							} else {
								pstmt.setString(param_position, (String) _params.get(_key));
							}
							_ifParamSet = true;
						} catch (SQLException sqle) {
							_ifParamSet = false;
							_log.error("SQLException:" + sqle.getMessage());
							throw new VException("Error: Unable to set values for key " + _key + " ,as " + sqle.getMessage());
						} catch (Exception ex) {
							_ifParamSet = false;
							_log.error("Exception:" + ex.getMessage());
							throw new VException("Error: Unable to set values for key " + _key + " ,as " + ex.getMessage());
						}
					} else {
						_ifParamSet = false;
						_log.error("data type value is not present");
						throw new VException("There is no data type defined in property file for " + _key + ".");
					}
				} else {
					_ifParamSet = false;
					_log.error("value is not present in provided hashmap for the key " + _key);
					throw new VException("There is no key in hashmap for" + _key + ".");
				}
				_token = _key = data_type = null;
			}//end of while
			_st = null;
		} else {
			_ifParamSet = true;
			_log.info("There is no params set in property file");
		}
		query_params = null;
		_log.info("exiting setParams method");
		return _ifParamSet;
	}
	/*
	 * Method to create
	 * _queryID : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	@Override
	public int create(String _queryID, HashMap _params) throws VException {
		_log.info("inside create method: ");
		int _retval = 0;
		Statement statement = null;
		try {
			statement = _con.createStatement();
			String _query = fetchQuery(_queryID, _params);
			_retval = statement.executeUpdate(_query);
			_log.debug("value from database is:" + _retval);
			_query = null;
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in create(): " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in create(): " + e.getMessage() + " Please contact administrator");
		} finally {
			if (statement != null) {
				try {
					statement.close();
					statement = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("Error in create(): " + e.getMessage() + " Please contact administrator");
				}
			}
		}
		_log.info("exiting create method");
		return _retval;
	}
	/*
	 * Method to insert data in a table
	 * _query : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	@Override
	public int insert(String _queryID, HashMap _params) throws VException {
		_log.info("inside insert method: ");
		PreparedStatement pstmt = null;
		int _retval = 0;
		try {
			String _query = fetchQuery(_queryID, _params);
			pstmt = _con.prepareStatement(_query);
			boolean _ifParamSet = setParams(_queryID, pstmt, _params);
			if (_ifParamSet) {
				_retval = pstmt.executeUpdate();
				_log.debug("Value from database is:" + _retval);
			}
			_query = null;
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in insert(): " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in insert(): " + e.getMessage() + " Please contact administrator");
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("Error in insert(): " + e.getMessage() + " Please contact administrator");
				}
			}
		}
		_log.info("exiting insert method");
		return _retval;
	}
	/*
	 * Method to update table
	 * _queryID : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	@Override
	public int update(String _queryID, HashMap _params) throws VException {
		_log.info("inside update method: ");
		PreparedStatement pstmt = null;
		int _retval = 0;
		try {
			String _query = fetchQuery(_queryID, _params);
			pstmt = _con.prepareStatement(_query);
			boolean _ifParamSet = setParams(_queryID, pstmt, _params);
			if (_ifParamSet) {
				_retval = pstmt.executeUpdate();
				_log.debug("Value from database is:" + _retval);
			}
			_query = null;
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in update(): " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in update(): " + e.getMessage() + " Please contact administrator");
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("Error in update(): " + e.getMessage() + " Please contact administrator");
				}
			}
		}
		_log.info("exiting update method");
		return _retval;
	}
	/*
	 * Method to retrieve data from a table
	 * _query : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	/*
	 * This will parse the date or timestamp into a defined format
	 * format will be defined in Query.properties
	 * date_time : Date or Time to be formatted
	 * _type : tell us if it is a Date or TimeStamp
	 */

	public Date parseIntoDate(String date_time, String _type) throws VException {
		_log.info("inside parseIntoDate method: ");
		Date util_date = null;
		try {
			String sdf_format = null;
			if (_type.equals("D")) {
				sdf_format = ResourceBundle.getBundle("com/miles/dao/Query").getString("date-display-format");
			} else {
				sdf_format = ResourceBundle.getBundle("com/miles/dao/Query").getString("timestamp-display-format");
			}
			SimpleDateFormat _sdf = new SimpleDateFormat(sdf_format);
			if (date_time.equals("") || date_time == null) {
				util_date = null;
			} else {
				util_date = _sdf.parse(date_time);
			}
			
			System.out.println("formatted type is:====== " + sdf_format);
			System.out.println("formatted type in parseIntoDate is: " + util_date);
			_log.debug("formatted type is: " + sdf_format);
			_log.debug("formatted type in parseIntoDate is: " + util_date);
			_sdf = null;
			sdf_format = null;
		} catch (ParseException ex) {
			_log.error("ParseException:" + ex.getMessage());
			throw new VException("Unable to parse Date. " + ex.getMessage());
		} catch (Exception ex) {
			_log.error("Exception:" + ex.getMessage());
			throw new VException("Exception in parseIntoDate(): " + ex.getMessage());
		}
		_log.info("exiting parseIntoDate method");
		return util_date;
	}

	@SuppressWarnings("finally")
	@Override
	public Vector retrieve(String _queryID, HashMap _params) throws VException {

		HashMap _map = null;
		Vector _vector = new Vector();
		PreparedStatement pstmt = null;
		ResultSetMetaData rsmd = null;
		ResultSet resultset = null;
		try {
			String _query = fetchQuery(_queryID, _params);
			System.out.println("inside retrieve method.  "+ _query);
			System.out.println("Params" + _params);

			pstmt = _con.prepareStatement(_query);

			boolean _ifParamSet = setParams(_queryID, pstmt, _params);
			_log.debug("reponse from params" + _ifParamSet);
			if (_ifParamSet) {
				resultset = pstmt.executeQuery();
				_log.debug("After getting resultset");
				rsmd = resultset.getMetaData();
				_log.debug("After getting resultsetmetadata");
				while (resultset.next()) {
					_map = new HashMap();
					for (int next = 1; next <= rsmd.getColumnCount(); next++) {
						String column_name = rsmd.getColumnName(next);
						_log.debug("column name is: " + column_name);

						String column_value = resultset.getString(rsmd.getColumnName(next));
						if (column_value != null) {
							column_value = column_value.trim();
						}

						_log.debug("column value is: " + column_value);

						_map.put(column_name, column_value);
					}
					_vector.add(_map);

				}
				_log.debug("Value from database is:" + _vector);
			}
			_query = null;
		} catch (ClassCastException c) {
			_log.error("ClassCastException:" + c.getMessage());
			throw new VException("ClassCastException in retrieve(): " + c.getMessage() + " Please contact administrator");
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in retrieve(): " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			e.printStackTrace();
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in retrieve(): " + e.getMessage() + " Please contact administrator");
		} finally {
			if (resultset != null) {
				try {
					resultset.close();
					resultset = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("resultset not closed in retrieve() " + e.getMessage() + " Please contact administrator");
				}
			}
			if (rsmd != null) {
				try {
					rsmd = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("resultsetmetadata not closed in retrieve(). Please contact administrator");
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("prparedstatement not closed in retrieve. Please contact administrator");
				}
			}
			_log.info("exiting retrieve method");
			return _vector;
		}
	}



	public HashMap retrieveValues(String _queryID, HashMap _params) throws VException {

		HashMap _map = null;
		PreparedStatement pstmt = null;
		ResultSetMetaData rsmd = null;
		ResultSet resultset = null;
		try {
			String _query = fetchQuery(_queryID, _params);
			System.out.println("inside retrieve method: " + _query);
			System.out.println("Params: " + _params);

			pstmt = _con.prepareStatement(_query);

			boolean _ifParamSet = setParams(_queryID, pstmt, _params);

			_log.debug("reponse from params" + _ifParamSet);
			if (_ifParamSet) {
				resultset = pstmt.executeQuery();

				_log.debug("After getting resultset");
				rsmd = resultset.getMetaData();
				_log.debug("After getting resultsetmetadata");
				_map = new HashMap();
				int i=0;
				while (resultset.next()) {

					for (int next = 1; next <= rsmd.getColumnCount(); next++) {
						String column_name = rsmd.getColumnName(next);
						_log.debug("column name is: " + column_name);

						String column_value = resultset.getString(rsmd.getColumnName(next));
						if (column_value != null) {
							column_value = column_value.trim();
						}

						_log.debug("column value is: " + column_value);
						_map.put(column_name+String.valueOf(i), column_value);
					}
					i++;

				}
				_log.debug("Value from database is added ");
			}
			_query = null;    
		} catch (ClassCastException c) {
			_log.error("ClassCastException:" + c.getMessage());
			throw new VException("ClassCastException in retrieve(): " + c.getMessage() + " Please contact administrator");
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in retrieve(): " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in retrieve(): " + e.getMessage() + " Please contact administrator");
		} finally {
			if (resultset != null) {
				try {
					resultset.close();
					resultset = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("resultset not closed in retrieve() " + e.getMessage() + " Please contact administrator");
				}
			}
			if (rsmd != null) {
				try {
					rsmd = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("resultsetmetadata not closed in retrieve(). Please contact administrator");
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("prparedstatement not closed in retrieve. Please contact administrator");
				}
			}
			_log.info("exiting retrieve method");
			return _map;
		}

	}



	/*
	 * Method to delete data from a table
	 * _query : queryID that represent query in Query.properties
	 * _params : HashMap in which all parameter exists
	 */

	@Override
	public int delete(String _queryID, HashMap _params) throws VException {
		_log.info("inside delete method");
		PreparedStatement pstmt = null;
		int _retval = 0;
		try {
			String _query = fetchQuery(_queryID, _params);
			pstmt = _con.prepareStatement(_query);
			boolean _ifParamSet = setParams(_queryID, pstmt, _params);
			if (_ifParamSet) {
				_retval = pstmt.executeUpdate();
				_log.debug("Value from database is:" + _retval);
			}
			_query = null;
		} catch (SQLException sqle) {
			_log.error("SQLException:" + sqle.getMessage());
			throw new VException("Error in delete() " + sqle.getMessage() + " Please contact administrator");
		} catch (Exception e) {
			_log.error("Exception:" + e.getMessage());
			throw new VException("Error in delete() " + e.getMessage() + " Please contact administrator");
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					_log.error("Exception:" + e.getMessage());
					throw new VException("Error in delete() " + e.getMessage() + " Please contact administrator");
				}
			}
		}
		_log.info("exiting delete method");
		return _retval;
	}

	/*
	 * ************************DEPRECATED METHODS *******************************
	 */
	/*
	 * Method to insert data in a table
	 * _query : query to execute
	 * _params : Vector in which all parameter exists
	 */
	public int insert(String _query, Vector _params) {
		PreparedStatement pstmt = null;
		Object _obj;
		int _retval = 0;
		try {
			pstmt = _con.prepareStatement(_query);
			for (int incr = 0; incr < _params.size(); incr++) {
				_obj = _params.get(incr);
				if (_obj instanceof String) {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				} else if (_obj instanceof Integer) {
					pstmt.setInt(incr + 1, (Integer) _params.get(incr));
				} else if (_obj instanceof Float) {
					pstmt.setFloat(incr + 1, (Float) _params.get(incr));
				} else if (_obj instanceof Double) {
					pstmt.setDouble(incr + 1, (Double) _params.get(incr));
				} else if (_obj instanceof Date) {
					pstmt.setDate(incr + 1, (java.sql.Date) _params.get(incr));
				} else if (_obj instanceof Timestamp) {
					pstmt.setTimestamp(incr + 1, (Timestamp) _params.get(incr));
				} else {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				}
			}
			_retval = pstmt.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return _retval;
	}
	/*
	 * Method to update table
	 * _query : query to execute
	 * _params : Vector in which all parameter exists
	 */

	public int update(String _query, Vector _params) {
		PreparedStatement pstmt = null;
		Object _obj;
		int _retval = 0;
		try {
			pstmt = _con.prepareStatement(_query);
			for (int incr = 0; incr < _params.size(); incr++) {
				_obj = _params.get(incr);
				if (_obj instanceof String) {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				} else if (_obj instanceof Integer) {
					pstmt.setInt(incr + 1, (Integer) _params.get(incr));
				} else if (_obj instanceof Float) {
					pstmt.setFloat(incr + 1, (Float) _params.get(incr));
				} else if (_obj instanceof Double) {
					pstmt.setDouble(incr + 1, (Double) _params.get(incr));
				} else if (_obj instanceof Date) {
					pstmt.setDate(incr + 1, (java.sql.Date) _params.get(incr));
				} else if (_obj instanceof Timestamp) {
					pstmt.setTimestamp(incr + 1, (Timestamp) _params.get(incr));
				} else {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				}
			}
			_retval = pstmt.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					System.out.println("Exception e : " + e.getMessage());
				}
			}
		}
		return _retval;
	}
	/*
	 * Method to insert data in a table
	 * _query : query to execute
	 * _params : Vector in which all parameter exists
	 */

	public Vector retrieve(String _query, Vector _params) {
		HashMap _map = null;
		Vector _vector = new Vector();
		PreparedStatement pstmt = null;
		ResultSetMetaData rsmd = null;
		ResultSet resultset = null;
		Object _obj;
		try {
			pstmt = _con.prepareStatement(_query);
			for (int incr = 0; incr < _params.size(); incr++) {
				_obj = _params.get(incr);
				if (_obj instanceof String) {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				} else if (_obj instanceof Integer) {
					pstmt.setInt(incr + 1, (Integer) _params.get(incr));
				} else if (_obj instanceof Float) {
					pstmt.setFloat(incr + 1, (Float) _params.get(incr));
				} else if (_obj instanceof Double) {
					pstmt.setDouble(incr + 1, (Double) _params.get(incr));
				} else if (_obj instanceof Date) {
					pstmt.setDate(incr + 1, (java.sql.Date) _params.get(incr));
				} else if (_obj instanceof Timestamp) {
					pstmt.setTimestamp(incr + 1, (Timestamp) _params.get(incr));
				} else {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				}
			}
			resultset = pstmt.executeQuery();
			resultset = pstmt.executeQuery();
			rsmd = resultset.getMetaData();
			while (resultset.next()) {
				_map = new HashMap();
				for (int next = 1; next <= rsmd.getColumnCount(); next++) {
					_map.put(rsmd.getColumnName(next), resultset.getString(rsmd.getColumnName(next)));
				}
				_vector.add(_map);
			}
		} catch (ClassCastException c) {
			System.out.println(c.getMessage());
		} catch (SQLException sqle) {
			System.out.println(sqle.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (resultset != null) {
				try {
					resultset.close();
					resultset = null;
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}

			if (rsmd != null) {
				try {
					rsmd = null;
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			return _vector;
		}
	}
	/*
	 * Method to delete data from a table
	 * _query : query to execute
	 * _params : Vector in which all parameter exists
	 */

	public int delete(String _query, Vector _params) {
		PreparedStatement pstmt = null;
		Object _obj;
		int _retval = 0;
		try {
			pstmt = _con.prepareStatement(_query);
			for (int incr = 0; incr < _params.size(); incr++) {
				_obj = _params.get(incr);
				if (_obj instanceof String) {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				} else if (_obj instanceof Integer) {
					pstmt.setInt(incr + 1, (Integer) _params.get(incr));
				} else if (_obj instanceof Float) {
					pstmt.setFloat(incr + 1, (Float) _params.get(incr));
				} else if (_obj instanceof Double) {
					pstmt.setDouble(incr + 1, (Double) _params.get(incr));
				} else if (_obj instanceof Date) {
					pstmt.setDate(incr + 1, (java.sql.Date) _params.get(incr));
				} else if (_obj instanceof Timestamp) {
					pstmt.setTimestamp(incr + 1, (Timestamp) _params.get(incr));
				} else {
					pstmt.setString(incr + 1, (String) _params.get(incr));
				}
			}
			_retval = pstmt.executeUpdate();
		} catch (SQLException sqle) {
			System.out.println("sqle Exception : " + sqle.getMessage());
		} catch (Exception e) {
			System.out.println("Exception  : " + e.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
					pstmt = null;
				} catch (Exception e) {
					System.out.println("Exception e : " + e.getMessage());
				}
			}
		}
		return _retval;
	}

	public void test1() {

		HashMap map = new HashMap();
		map.put("YUP", 2);
		map.put("adddress", "vik");
		map.put("TIM", "11-11-2008");
		map.put("DAYS", "11-11-2008");
		map.put("SALARY", "123");
		//map.put("YUP=?","velocis");
		try {
			openConnection();
			retrieve("query6", map);
			System.out.println("Hi");
			closeConnection();
		} catch (VException ex) {
			//ex.printStackTrace();
			System.out.println("x" + ex.getMessage());
		} catch (Exception ex) {
			System.out.println("ex" + ex.getMessage());
		} catch (Throwable ex) {
			System.out.println("ex" + ex.getMessage());
		}

	}

}


