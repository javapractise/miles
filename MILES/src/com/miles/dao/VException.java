/*
 * VException.java
 *
 * Created on October 7, 2008, 10:53 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.miles.dao;

/**
 *
 * @author
 */
public class VException extends Throwable{
    
    /*
     * Creates a new instance of VException
     */
    public VException() {
    }
    /*
     * Create a new instance with a message
     * _msg : Message to display
     */
    public VException(String _msg) {
        super(_msg);
    }
    /*
     * Create a new instance with a message
     * _msg : Message to display
     */
    public VException(String msg,Throwable t){
        super(msg,t);
        
    }
    
}
