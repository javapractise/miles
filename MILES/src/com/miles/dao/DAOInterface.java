package com.miles.dao;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author
 */
public interface DAOInterface {
    public boolean openConnection() throws VException;
    public void closeConnection() throws VException;
    public int insert(String _query, HashMap _params) throws VException;
    public int update(String _query, HashMap _params) throws VException;
    public Vector retrieve(String _query, HashMap _params) throws VException;
    public int delete(String _query, HashMap _params) throws VException;
    public int create(String _query, HashMap _params)throws VException;
}
